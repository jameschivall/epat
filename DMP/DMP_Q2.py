import pandas_datareader.data as web
import pandas as pd

start_date = '2000-01-01'
end_date = '2019-12-01'

df1 = web.DataReader('MSFT', 'yahoo')
df = pd.DataFrame(df1['Adj Close'])

df['ma'] = df['Adj Close'].rolling(200).mean()
df['ma_1'] = df['ma'].shift(1)
df['Adj Close_1'] = df['Adj Close'].shift(1)

df['buy_signal'] = df.apply(
    lambda row: (row['Adj Close'] > row['ma']) & (row['Adj Close_1'] < row['ma_1']),
    axis=1)
df['sell_signal'] = df.apply(
    lambda row: (row['Adj Close'] < row['ma']) & (row['Adj Close_1'] < row['ma_1']),
    axis=1)

df['signal'] = 0
df['signal'].loc[df['buy_signal']] = 1
df['signal'].loc[df['sell_signal']] = -1

df['signal_ff'] = df['signal'].replace(0, method='ffill')

df['returns'] = pd.np.log(df['Adj Close'] / df['Adj Close_1'])
df['returns'].loc[df['signal_ff'] < 1] = 0

df['cum_returns'] = df['returns'].cumsum()
